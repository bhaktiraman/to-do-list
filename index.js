var listArray = [];
var count = 0;

function refreshDOM() {
    document.querySelector('#list').innerHTML = "";
    var complete;
    listArray.forEach(element => {
        if (element.status == "none") {
            complete = "item";
        } else {
            complete = "item-checked";
        }
        const item = `<li class="${complete}" id="${element.id}">
                        <input type="checkbox" ${element.status} onclick="toggleTodoItemStatus(this.parentElement)">
                        <p>${element.text}</p>
                        <button onClick="deleteTodoItem(this.parentElement)">X</button>
                  </li>`;
        const position = "beforeend";
        list.insertAdjacentHTML(position, item);
    });
}

textInput.addEventListener("keypress", function(event) {
    document.getElementById("errorMessage").className = 'message';
    if (event.keyCode == 13) {
        const toDo = document.getElementById("textInput").value;
        if (toDo) {
            var newList = { id: count++, text: toDo, status: "none" };
            listArray.push(newList);
        } else {
            document.getElementById("errorMessage").className = 'errorMessage';
        }
        document.getElementById("textInput").value = "";
    }
    refreshDOM();
});

function deleteTodoItem(item) {
    listArray.forEach(element => {
        if (item.id == element.id) {
            listArray.splice(element.id, 1);
        }
    });
    refreshDOM();
}

function toggleTodoItemStatus(item) {
    listArray.forEach(element => {
        if (item.id == element.id) {
            if (element.status == "none") {
                element.status = "checked";
            } else {
                element.status = "none";
            }
        }
    });
    refreshDOM();
}